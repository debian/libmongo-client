libmongo-client (0.1.8-4) UNRELEASED; urgency=medium

  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/control: Remove trailing whitespaces
  * d/control: Set Vcs-* to salsa.debian.org

 -- Ondřej Nový <onovy@debian.org>  Mon, 01 Oct 2018 09:49:43 +0200

libmongo-client (0.1.8-3) unstable; urgency=medium

  * debian/libmongo-client-doc.maintscript:
    - Add missing symlink_to_dir calls for libmongo-client0-dbg
      and libmongo-client-dev (Closes: #860115).

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Tue, 11 Apr 2017 20:36:41 +0200

libmongo-client (0.1.8-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Don't use link-doc between arch:all and arch:any (Closes: #858375).

 -- Ivo De Decker <ivodd@debian.org>  Sat, 08 Apr 2017 21:57:18 +0200

libmongo-client (0.1.8-2) unstable; urgency=medium

  * New Maintainer (Closes: #770801).
  * debian/contol:
    - Set myself as maintainer.
    - Bump Standards-Version to 3.9.6 (no changes required).
    - Change Vcs-* to collab-maint since old Vcs is not longer available.
  * debian/copyright:
    - Add missing license for tests/tools/*.
    - Add myself to the list of authors for debian/*.
    - Change upstream email address to algernon@madhouse-project.org.
    - Change source to github.
  * debian/rules:
    - Remove useless override_dh_builddeb: xz is now standard.
    - Remove oldstyle debhelper parts.
    - Add override_dh_makeshlibs to build symbols on every build.
    - Add missing LDFLAGS.
  * debian/watch:
    - Change source to github.
  * Remove debian/source/options because xz is now standard.
  * New debian/patches/0100-gcc5.patch:
    - Cheery-picked upstream commit 7ccdf08e81aa51a1b54eb4c574e6e4fc5f2e2d54
      to make the library fit for gcc-5 (Closes: #777959).

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Wed, 08 Jul 2015 14:42:01 +0200

libmongo-client (0.1.8-1) unstable; urgency=medium

  * New upstream release.
  * debian/symbols updated with the new LMC_0.1.8 version tag.
  * debian/copyright updated to include 2014 in copyright years.
  * debian/rules updated to drop --with-versioned-symbols: it is
    automatically enabled now, if supported.

 -- Gergely Nagy <algernon@madhouse-project.org>  Thu, 22 May 2014 13:05:18 +0200

libmongo-client (0.1.7.1-1) unstable; urgency=medium

  * New upstream release.
    + Improves the roboustness of safe mode.

 -- Gergely Nagy <algernon@madhouse-project.org>  Fri, 14 Mar 2014 11:04:43 +0100

libmongo-client (0.1.7-1) unstable; urgency=low

  * New upstream release.
    + Automatic re-authentication support, even when using automatic
      failover with replicasets.
  * debian/symbols updated with the new LMC_0.1.7 version tag.

 -- Gergely Nagy <algernon@madhouse-project.org>  Sun, 15 Dec 2013 11:37:03 +0100

libmongo-client (0.1.6.3-2) unstable; urgency=low

  * debian/copyright: Point to /usr/share/common-licenses/Apache-2.0 for
    the full text of the license. (Thanks, lintian!)
  * Bumped Standards-Version to 3.9.5, no changed necessary.

 -- Gergely Nagy <algernon@madhouse-project.org>  Tue, 05 Nov 2013 10:38:33 +0100

libmongo-client (0.1.6.3-1) unstable; urgency=low

  * New upstream release.
    + Fixed chunked GridFS file retrieval, to keep the chunk order.
    + Fixed a crash that happened when verification in safe-mode failed.
  * Bumped Standards-Version to 3.9.4, no changes necessary.
  * debian/copyright: Updated the copyright years.

 -- Gergely Nagy <algernon@madhouse-project.org>  Tue, 27 Aug 2013 10:10:37 +0200

libmongo-client (0.1.6.2-1) unstable; urgency=low

  * New upstream release.
    + Minor memory leak fix in the GridFS code
    + Support for the obsolete Binary subtype in GridFS files
    + Documentation improvements
  * Don't muck with m4/ during build, it is included upstream now.

 -- Gergely Nagy <algernon@madhouse-project.org>  Fri, 21 Dec 2012 12:35:43 +0100

libmongo-client (0.1.6.1-3) unstable; urgency=low

  * Make libmongo-client-dev depend on libglib2.0-dev. Thanks Michael
    Biebl. (Closes: #690992)

 -- Gergely Nagy <algernon@madhouse-project.org>  Sat, 20 Oct 2012 08:30:09 +0200

libmongo-client (0.1.6.1-2) unstable; urgency=low

  * Really build with verbose messages enabled, the test suite too.
  * Install and clean the m4/ directory too during build, to make
    backporting easier.

 -- Gergely Nagy <algernon@madhouse-project.org>  Mon, 15 Oct 2012 08:38:07 +0200

libmongo-client (0.1.6.1-1) unstable; urgency=low

  * New upstream bugfix release.
    + Restores ABI compatibility with versions prior to 0.1.6.

 -- Gergely Nagy <algernon@madhouse-project.org>  Sun, 14 Oct 2012 23:15:47 +0200

libmongo-client (0.1.6-1) unstable; urgency=low

  * New upstream release.
  * Build with verbose messages enabled.
  * Use xz compression for source & binaries too.

 -- Gergely Nagy <algernon@madhouse-project.org>  Sun, 14 Oct 2012 21:18:39 +0200

libmongo-client (0.1.5-1) unstable; urgency=low

  * New upstream bugfix release.
    + Fixes build on s390x, sparc64 and ppc64.
  * Bump debhelper build-dependency to >= 9~
  * Update debian/copyright to copyright-format-1.0.
  * Bump Standards-Version to 3.9.3 (no other changes necessary)

 -- Gergely Nagy <algernon@madhouse-project.org>  Fri, 13 Apr 2012 18:37:21 +0200

libmongo-client (0.1.4-3) unstable; urgency=low

  * Build-Depend on debhelper (>= 8.9.7~) for -arch/-indep override
    support.
  * Move doxygen & graphviz to Build-Depends-Indep, and only build docs
    when building -indep. This'll save quite a bit of disk space on
    buildds. Thanks Laszlo Boszormenyi <gcs@debian.hu> for the suggestion.

 -- Gergely Nagy <algernon@madhouse-project.org>  Wed, 14 Sep 2011 23:08:53 +0200

libmongo-client (0.1.4-2) unstable; urgency=low

  * Build a libmongo-client-doc package, with API docs, the tutorial and
    examples. Based on a patch from Guido Günther <agx@sigxcpu.org>
    (Closes: #639940).

 -- Gergely Nagy <algernon@madhouse-project.org>  Sat, 03 Sep 2011 23:55:10 +0200

libmongo-client (0.1.4-1) unstable; urgency=low

  * New upstream release.

 -- Gergely Nagy <algernon@madhouse-project.org>  Sat, 27 Aug 2011 13:26:17 +0200

libmongo-client (0.1.3-1) unstable; urgency=low

  * New upstream release.

 -- Gergely Nagy <algernon@madhouse-project.org>  Tue, 19 Jul 2011 19:48:33 +0200

libmongo-client (0.1.2-1) unstable; urgency=low

  * New upstream release.
  * Converted to multi-arch.
  * Use DEP-5 format debian/copyright.

 -- Gergely Nagy <algernon@madhouse-project.org>  Fri, 01 Jul 2011 11:08:57 +0200

libmongo-client (0.1.1-1) unstable; urgency=low

  * New upstream release.

 -- Gergely Nagy <algernon@madhouse-project.org>  Thu, 16 Jun 2011 12:25:16 +0200

libmongo-client (0.1.0-1) unstable; urgency=low

  * Initial release (Closes: #626969)

 -- Gergely Nagy <algernon@madhouse-project.org>  Wed, 25 May 2011 11:00:05 +0200
